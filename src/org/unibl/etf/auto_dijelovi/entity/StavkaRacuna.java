package org.unibl.etf.auto_dijelovi.entity;

public class StavkaRacuna {
    private int kupljenaKolicina;
    private double cijenaStavke;
    private String naziv;
    private int sifra;
    private double cijena;


    public StavkaRacuna(int kupljenaKolicina, double cijenaStavke, String naziv, int sifra, double cijena) {
        this.cijenaStavke = cijenaStavke;
        this.kupljenaKolicina = kupljenaKolicina;
        this.naziv = naziv;
        this.sifra = sifra;
        this.cijena = cijena;
    }

    public StavkaRacuna(){}

    public int getKupljenaKolicina() {
        return kupljenaKolicina;
    }

    public void setKupljenaKolicina(int kupljenaKolicina) {
        this.kupljenaKolicina = kupljenaKolicina;
    }

    public double getCijenaStavke() {
        return cijenaStavke;
    }

    public void setCijenaStavke(double cijenaStavke) {
        this.cijenaStavke = cijenaStavke;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getSifra() {
        return sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }

    public double getCijena() {
        return cijena;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }
}
