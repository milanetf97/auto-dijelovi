package org.unibl.etf.auto_dijelovi.entity;

public class Prodavac extends Radnik{
    private String korisnickoIme;
    private String lozinka;

    public Prodavac(String jmb, String ime, String prezime, String korisnickoIme, String lozinka){
        super(jmb, ime, prezime);

        this.korisnickoIme=korisnickoIme;
        this.lozinka=lozinka;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    @Override
    public String toString() {
        return "Prodavac{" +
                "korisnickoIme='" + korisnickoIme + '\'' +
                ", lozinka='" + lozinka + '\'' +
                '}';
    }
}
