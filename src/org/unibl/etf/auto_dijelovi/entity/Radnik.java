package org.unibl.etf.auto_dijelovi.entity;

public class Radnik {
    private String jmb;
    private String ime;
    private String prezime;

    public Radnik(){

    }

    public Radnik(String jmb, String ime, String prezime){
        super();
        this.ime=ime;
        this.prezime=prezime;
        this.jmb=jmb;
    }

    public String getJmb() {
        return jmb;
    }

    public void setJmb(String jmb) {
        this.jmb = jmb;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }
}
