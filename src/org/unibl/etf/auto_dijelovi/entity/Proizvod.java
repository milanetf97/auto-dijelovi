package org.unibl.etf.auto_dijelovi.entity;

public class Proizvod {
    private int idProizvoda;
    private String naziv;
    private int kolicina;
    private double cijena;
    private String imeKategorije;

    public Proizvod(int idProizvoda, String naziv, int kolicina, double cijena, String imeKategorije)
    {
        this.idProizvoda=idProizvoda;
        this.cijena=cijena;
        this.naziv=naziv;
        this.kolicina=kolicina;
        this.imeKategorije=imeKategorije;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getIdProizvoda() {
        return idProizvoda;
    }

    public void setIdProizvoda(int idProizvoda) {
        this.idProizvoda = idProizvoda;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public double getCijena() {
        return cijena;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public String getImeKategorije() {
        return imeKategorije;
    }

    public void setImeKategorije(String imeKategorije) {
        this.imeKategorije = imeKategorije;
    }

    @Override
    public String toString() {
        return "Proizvod{" +
                "idProizvoda=" + idProizvoda +
                ", naziv='" + naziv + '\'' +
                ", kolicinaKg=" + kolicina +
                ", cijenaPoKg=" + cijena +
                ", imeKategorije='" + imeKategorije + '\'' +
                '}';
    }
}
