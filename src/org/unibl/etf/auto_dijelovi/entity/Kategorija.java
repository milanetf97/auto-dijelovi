package org.unibl.etf.auto_dijelovi.entity;

public class Kategorija {
    private String naziv;
    private int idKategorije;

    public Kategorija(String naziv, int idKategorije) {
        this.naziv=naziv;
        this.idKategorije=idKategorije;
    }
}
