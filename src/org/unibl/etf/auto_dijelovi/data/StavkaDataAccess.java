package org.unibl.etf.auto_dijelovi.data;

public interface StavkaDataAccess {
    public void dodajStavku(int sifraProizvoda, int kupljenaKolicina, int racunId);
    public void obrisiStavku(int sifraProizvoda, int kupljenaKolicina, double cijenaStavke, int racunId);

}
