package org.unibl.etf.auto_dijelovi.data;

import org.unibl.etf.auto_dijelovi.entity.Proizvod;
import org.unibl.etf.auto_dijelovi.gui.StavkaPrikaz;

import java.util.List;

public interface ProizvodDataAccess {
    default List<Proizvod> izlistajProizvode() {
        return null;
    }

    default StavkaPrikaz proizvodInfo(int sifraProizvoda)
    {
        return null;
    }
}
