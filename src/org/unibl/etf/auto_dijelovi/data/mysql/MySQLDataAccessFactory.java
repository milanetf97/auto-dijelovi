package org.unibl.etf.auto_dijelovi.data.mysql;

import org.unibl.etf.auto_dijelovi.data.*;

public class MySQLDataAccessFactory extends DataAccessFactory {

	@Override
	public ProizvodDataAccess getProizvodDataAccess() {
		return new ProizvodDataAccessImpl();
	}

	@Override
	public RacunDataAccess getRacunDataAccess() {
		return new RacunDataAccessImpl();
	}

	@Override
	public RadnikDataAccess getRadnikDataAccess() {
		return new RadnikDataAccessImpl();
	}

	@Override
	public StavkaDataAccess getStavkaDataAccess() {
		return new StavkaDataAccessImpl();
	}

	@Override
	public ProdavacDataAccess getProdavacDataAccess() {return  new ProdavacDataAccessImpl(); }
}
