package org.unibl.etf.auto_dijelovi.data.mysql;

import org.unibl.etf.auto_dijelovi.data.StavkaDataAccess;

import java.sql.*;

public class StavkaDataAccessImpl implements StavkaDataAccess {
    @Override
    public void dodajStavku(int sifraProizvoda, int kupljenaKolicina, int racunId) {
        Connection conn = null;
        CallableStatement cs = null;

        String query = "{call dodaj_stavku(?, ?, ?)}";

        try{
            conn = ConnectionPool.getInstance().checkOut();
            cs = conn.prepareCall(query);

            cs.setInt(1, sifraProizvoda);
            cs.setInt(2, kupljenaKolicina);
            cs.setInt(3, racunId);

            cs.execute();

        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(cs);
        }
    }

    @Override
    public void obrisiStavku(int sifraProizvoda, int kupljenaKolicina, double cijenaStavke, int racunId) {
        Connection conn = null;
        CallableStatement cs = null;

        String query = "{call obrisi_stavku(?, ?, ?, ?)}";

        try{
            conn = ConnectionPool.getInstance().checkOut();
            cs = conn.prepareCall(query);

            cs.setInt(1, sifraProizvoda);
            cs.setInt(2, kupljenaKolicina);
            cs.setDouble(3, cijenaStavke);
            cs.setInt(4, racunId);

            cs.execute();

        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(cs);
        }
    }
}
