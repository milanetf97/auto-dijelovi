package org.unibl.etf.auto_dijelovi.data.mysql;

import org.unibl.etf.auto_dijelovi.data.RadnikDataAccess;
import org.unibl.etf.auto_dijelovi.entity.Radnik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RadnikDataAccessImpl implements RadnikDataAccess {
    @Override
    public List<Radnik> izlistajRadnike(){
        List<Radnik> retVal = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "select * from radnik";

        try{
            conn = ConnectionPool.getInstance().checkOut();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()){
                retVal.add(new Radnik(rs.getString(1),rs.getString(2), rs.getString(3)));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(ps);
        }

        return retVal;
    }

}
