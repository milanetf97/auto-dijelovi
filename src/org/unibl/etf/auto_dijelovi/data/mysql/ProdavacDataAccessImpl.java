package org.unibl.etf.auto_dijelovi.data.mysql;

import org.unibl.etf.auto_dijelovi.data.ProdavacDataAccess;
import org.unibl.etf.auto_dijelovi.entity.Prodavac;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProdavacDataAccessImpl implements ProdavacDataAccess {

    @Override
    public List<Prodavac> izlistajProdavce() {
        List<Prodavac> retVal = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "select p.RADNIK_jmb, r.ime, r.prezime, p.korisnicko_ime, p.lozinka from prodavac p\n"+
                       "inner join radnik r on r.jmb = RADNIK_jmb;";

        try{
            conn = ConnectionPool.getInstance().checkOut();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()){
                retVal.add(new Prodavac(rs.getString(1),rs.getString(2),
                                        rs.getString(3),rs.getString(4),rs.getString(5)));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(ps);
        }

        return retVal;
    }
}
