package org.unibl.etf.auto_dijelovi.data.mysql;

import org.unibl.etf.auto_dijelovi.data.ProizvodDataAccess;
import org.unibl.etf.auto_dijelovi.entity.*;
import org.unibl.etf.auto_dijelovi.gui.StavkaPrikaz;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProizvodDataAccessImpl implements ProizvodDataAccess {

    @Override
    public List<Proizvod> izlistajProizvode(){
        List<Proizvod> retVal = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "select p.sifra, p.naziv, p.kolicina, p.cijena, k.naziv as kategorija from proizvod p\n"+
                       "inner join kategorija k on p.KATEGORIJA_id_kategorije = k.id_kategorije\n"+
                       "group by p.sifra;";

        try{
            conn = ConnectionPool.getInstance().checkOut();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()){
                retVal.add(new Proizvod(rs.getInt(1),rs.getString(2),rs.getInt(3)
                                       ,rs.getDouble(4),rs.getString(5)));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(ps);
        }

        return retVal;
    }

    @Override
    public StavkaPrikaz proizvodInfo(int idProizvoda) {

        StavkaPrikaz retVal = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "select naziv, cijena from proizvod\n" +
                       "where sifra ="+Integer.toString(idProizvoda)+";";

        try {
            conn = ConnectionPool.getInstance().checkOut();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            if(rs.next()) {
                retVal = new StavkaPrikaz(idProizvoda, rs.getString(1), -1
                        , rs.getDouble(2),-1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(ps);
        }

        return retVal;
    }
}
