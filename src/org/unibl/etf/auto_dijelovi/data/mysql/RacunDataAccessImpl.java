package org.unibl.etf.auto_dijelovi.data.mysql;

import org.unibl.etf.auto_dijelovi.app.Main;
import org.unibl.etf.auto_dijelovi.data.RacunDataAccess;

import java.sql.*;

public class RacunDataAccessImpl implements RacunDataAccess {
    @Override
    public int izdajRacun()
    {
        int retVal = 0;
        Connection conn = null;
        CallableStatement cs = null;

        String query = "{call dodaj_racun(?, ?, ?)}";

        try{
            conn = ConnectionPool.getInstance().checkOut();
            cs = conn.prepareCall(query);
            cs.setDate(1, new Date(System.currentTimeMillis()));
            cs.setString(2, Main.trenutniKorisnik.getJmb());
            cs.registerOutParameter(3, Types.INTEGER);

            cs.execute();

            retVal = cs.getInt(3);

        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(cs);
        }

        return retVal;
    }

    @Override
    public void obrisiRacun(int idRacuna) {
        Connection conn = null;
        CallableStatement cs = null;

        String query = "{call obrisi_racun(?)}";

        try{
            conn = ConnectionPool.getInstance().checkOut();
            cs = conn.prepareCall(query);
            cs.setInt(1, idRacuna);

            cs.execute();

        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionPool.getInstance().checkIn(conn);
            MySQLUtilities.getInstance().close(cs);
        }
    }
}
