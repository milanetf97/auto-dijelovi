package org.unibl.etf.auto_dijelovi.data;

public interface RacunDataAccess {
    public default int izdajRacun() {
        return 0;
    }

    public default void obrisiRacun(int idRacuna)
    {
        return;
    }

}
