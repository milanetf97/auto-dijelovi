package org.unibl.etf.auto_dijelovi.data;

import org.unibl.etf.auto_dijelovi.data.mysql.MySQLDataAccessFactory;

public abstract class DataAccessFactory {
	
	public abstract ProizvodDataAccess getProizvodDataAccess();
	public abstract RacunDataAccess getRacunDataAccess();
	public abstract RadnikDataAccess getRadnikDataAccess();
	public abstract StavkaDataAccess getStavkaDataAccess();

	public abstract ProdavacDataAccess getProdavacDataAccess();

	public static DataAccessFactory getFactory(DataAccessFactoryType type) {
		if (DataAccessFactoryType.MySQL.equals(type)) {
			return new MySQLDataAccessFactory();
		}
		throw new IllegalArgumentException();
	}
	
}
