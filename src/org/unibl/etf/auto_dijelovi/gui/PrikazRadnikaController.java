package org.unibl.etf.auto_dijelovi.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.unibl.etf.auto_dijelovi.data.mysql.RadnikDataAccessImpl;
import org.unibl.etf.auto_dijelovi.entity.Radnik;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PrikazRadnikaController implements Initializable {
    @FXML
    private TableColumn<Radnik, String> KolonaIme;

    @FXML
    private TableColumn<Radnik, String> KolonaJMB;

    @FXML
    private TableColumn<Radnik, String> KolonaPrezime;

    @FXML
    private TableView<Radnik> TabelaRadnika;

    void izlistajSveRadnike() {
        List<Radnik> listaRadnika = new RadnikDataAccessImpl().izlistajRadnike();

        KolonaIme.setCellValueFactory(new PropertyValueFactory<Radnik,String>("ime"));
        KolonaPrezime.setCellValueFactory(new PropertyValueFactory<Radnik, String>("prezime"));
        KolonaJMB.setCellValueFactory(new PropertyValueFactory<Radnik, String>("jmb"));

        ObservableList<Radnik> oListRadnik = FXCollections.observableArrayList(listaRadnika);
        TabelaRadnika.setItems(oListRadnik);
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        izlistajSveRadnike();
    }
}
