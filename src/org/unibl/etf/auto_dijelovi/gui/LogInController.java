package org.unibl.etf.auto_dijelovi.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.unibl.etf.auto_dijelovi.app.Main;
import org.unibl.etf.auto_dijelovi.data.mysql.ProdavacDataAccessImpl;
import org.unibl.etf.auto_dijelovi.entity.Prodavac;

import java.util.List;
import java.util.stream.Collectors;

public class LogInController {

    @FXML
    private TextField ImeTextField;

    @FXML
    private PasswordField LozinkaTextFiled;

    @FXML
    void logIn(ActionEvent event) {

        try {
            String ime;
            String lozinka;

            Prodavac prodavac = null;
            ime = ImeTextField.getText();
            lozinka = LozinkaTextFiled.getText();

            List<Prodavac> list = new ProdavacDataAccessImpl().izlistajProdavce();

            List<Prodavac> p = list.stream()
                    .filter((Prodavac pp) -> pp.getKorisnickoIme().equals(ime) && pp.getLozinka().equals(lozinka))
                    .collect(Collectors.toList());

            prodavac = p.get(0);

            if ((prodavac != null) && (Main.trenutniKorisnik == null)) {
                Main.trenutniKorisnik = prodavac;

                FXMLLoader fxmlLoader = new FXMLLoader(LogInController.class.getResource("..\\gui\\MainMenuGUI.fxml"));
                Stage stage = new Stage();
                Scene scene = new Scene(fxmlLoader.load(), 600, 400);
                stage.setTitle("Auto dijelovi");
                stage.setScene(scene);
                stage.show();
                ((Stage) ImeTextField.getScene().getWindow()).close();
            }
        }catch (Exception e)
        {
            System.out.println("Pokusajte ponovo");
        }
    }
}
