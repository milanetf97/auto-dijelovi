package org.unibl.etf.auto_dijelovi.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.unibl.etf.auto_dijelovi.data.mysql.ProizvodDataAccessImpl;
import org.unibl.etf.auto_dijelovi.data.mysql.RacunDataAccessImpl;
import org.unibl.etf.auto_dijelovi.data.mysql.StavkaDataAccessImpl;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class RacunController implements Initializable {

    private static int idNovogRacuna;
    private static boolean racunIzdat = false;
    ObservableList<StavkaPrikaz> oListStavkaPrikaz = FXCollections.observableArrayList();
    @FXML
    private TextField SifraProizvodaTextField;
    @FXML
    private TextField KolicinaTextField;

    @FXML
    private Text PrikazText;

    @FXML
    private TableView<StavkaPrikaz> RacunTabela;

    @FXML
    private TableColumn<StavkaPrikaz, String> NazivKolona;

    @FXML
    private TableColumn<StavkaPrikaz, Integer> SifraKolona;

    @FXML
    private TableColumn<StavkaPrikaz, Double> cijenaPoKomKolona;

    @FXML
    private TableColumn<StavkaPrikaz, Integer> kolicinaKolona;

    @FXML
    private TableColumn<StavkaPrikaz, Double> ukupnaCijenaKolona;

    @FXML
    void dodajStavku(ActionEvent event) {

        try {
            int sifraProizvoda = Integer.parseInt(SifraProizvodaTextField.getText());
            int kolicinaProizvoda = Integer.parseInt(KolicinaTextField.getText());
            double trenutniIznos = Double.parseDouble(PrikazText.getText());

            StavkaPrikaz p = new ProizvodDataAccessImpl().proizvodInfo(sifraProizvoda);

            p.setKolicina(kolicinaProizvoda);
            p.setUkupnaCijena(kolicinaProizvoda * p.getCijenaPoKom());

            List<StavkaPrikaz> sp = oListStavkaPrikaz.stream()
                    .filter(s -> s.getSifra() == p.getSifra())
                    .collect(Collectors.toList());

            if (sp.isEmpty()) {
                oListStavkaPrikaz.add(p);
            } else {
                //System.out.println(sp.get(0).getKolicina());
                sp.get(0).setKolicina(sp.get(0).getKolicina() + p.getKolicina());
                //System.out.println(sp.get(0).getKolicina());
                sp.get(0).setUkupnaCijena(sp.get(0).getUkupnaCijena() + p.getUkupnaCijena());
            }

            PrikazText.setText(Double.toString(trenutniIznos + p.getUkupnaCijena()));

            RacunTabela.refresh();
        }
        catch (Exception e)
        {
            System.out.println("Nepostojeci ili nevalidni podaci za proizvod koji se dodaje na racun");
        }
    }

    @FXML
    void izdajRacun(ActionEvent event) {
        idNovogRacuna = new RacunDataAccessImpl().izdajRacun();
        StavkaDataAccessImpl ri = new StavkaDataAccessImpl();
        oListStavkaPrikaz.stream()
                         .forEach( s-> ri.dodajStavku(s.getSifra(), s.getKolicina(),idNovogRacuna));
        ((Stage) RacunTabela.getScene().getWindow()).close();
    }

    @FXML
    void obrisiStavku(ActionEvent event) {
        StavkaPrikaz s = RacunTabela.getSelectionModel().getSelectedItem();
        double trenutniIznos = Double.parseDouble(PrikazText.getText());

        RacunTabela.getItems().remove(s);
        oListStavkaPrikaz.remove(s);
        PrikazText.setText(Double.toString(trenutniIznos-s.getUkupnaCijena()));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        PrikazText.setText("0.00");

        NazivKolona.setCellValueFactory(new PropertyValueFactory<StavkaPrikaz, String>("naziv"));
        SifraKolona.setCellValueFactory(new PropertyValueFactory<StavkaPrikaz, Integer>("sifra"));
        cijenaPoKomKolona.setCellValueFactory(new PropertyValueFactory<StavkaPrikaz, Double>("cijenaPoKom"));
        ukupnaCijenaKolona.setCellValueFactory(new PropertyValueFactory<StavkaPrikaz, Double>("ukupnaCijena"));
        kolicinaKolona.setCellValueFactory(new PropertyValueFactory<StavkaPrikaz, Integer>("kolicina"));
        RacunTabela.setItems(oListStavkaPrikaz);

    }

    public static int getIdNovogRacuna() {
        return idNovogRacuna;
    }

    public static void setIdNovogRacuna(int idNovogRacuna) {
        RacunController.idNovogRacuna = idNovogRacuna;
    }

    public static boolean isRacunIzdat() {
        return racunIzdat;
    }

    public static void setRacunIzdat(boolean racunIzdat) {
        RacunController.racunIzdat = racunIzdat;
    }
}
