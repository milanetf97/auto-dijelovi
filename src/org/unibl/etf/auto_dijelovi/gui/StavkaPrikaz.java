package org.unibl.etf.auto_dijelovi.gui;

public class StavkaPrikaz{
    private int sifra;
    private String naziv;
    private int kolicina;
    private double cijenaPoKom;
    private double ukupnaCijena;

    public StavkaPrikaz(int sifra, String naziv, int kolicina, double cijenaPoKom, double ukupnaCijena) {
        this.sifra = sifra;
        this.naziv = naziv;
        this.kolicina = kolicina;
        this.cijenaPoKom = cijenaPoKom;
        this.ukupnaCijena = ukupnaCijena;
    }

    public int getSifra() {
        return sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public double getCijenaPoKom() {
        return cijenaPoKom;
    }

    public void setCijenaPoKom(double cijenaPoKom) {
        this.cijenaPoKom = cijenaPoKom;
    }

    public double getUkupnaCijena() {
        return ukupnaCijena;
    }

    public void setUkupnaCijena(double ukupnaCijena) {
        this.ukupnaCijena = ukupnaCijena;
    }
}
