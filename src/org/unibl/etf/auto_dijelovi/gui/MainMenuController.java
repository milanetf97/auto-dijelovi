package org.unibl.etf.auto_dijelovi.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainMenuController {

    @FXML
    void izlistajProizvode(ActionEvent event) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(MainMenuController.class.getResource("..\\gui\\PrikazProizvodaGUI.fxml"));
        Stage stage= new Stage();
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Izlistaj proizvode");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void izlistajRadnike(ActionEvent event) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(PrikazRadnikaController.class.getResource("..\\gui\\PrikazRadnikaGUI.fxml"));
        Stage stage= new Stage();
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Izlistaj radnike");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void kreirajRacun(ActionEvent event) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(MainMenuController.class.getResource("..\\gui\\RacunGUI.fxml"));
        Stage stage= new Stage();
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Kreiranje racuna");
        stage.setScene(scene);

        stage.show();
    }

}
