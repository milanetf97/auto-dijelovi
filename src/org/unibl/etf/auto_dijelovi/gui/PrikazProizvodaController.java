package org.unibl.etf.auto_dijelovi.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.unibl.etf.auto_dijelovi.data.mysql.ProizvodDataAccessImpl;
import org.unibl.etf.auto_dijelovi.entity.Proizvod;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PrikazProizvodaController implements Initializable {

    @FXML
    private TableColumn<Proizvod, Double> kolonaCijena;

    @FXML
    private TableColumn<Proizvod, String> kolonaKategorija;

    @FXML
    private TableColumn<Proizvod, Integer> kolonaKolicina;

    @FXML
    private TableColumn<Proizvod, String> kolonaNaziv;

    @FXML
    private TableColumn<Proizvod, Integer> kolonaSifra;

    @FXML
    private TableView<Proizvod> tabelaProizvoda;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        List<Proizvod> listaProizvoda = new ProizvodDataAccessImpl().izlistajProizvode();

        kolonaCijena.setCellValueFactory(new PropertyValueFactory<Proizvod, Double>("cijena"));
        kolonaKategorija.setCellValueFactory(new PropertyValueFactory<Proizvod, String>("imeKategorije"));
        kolonaKolicina.setCellValueFactory(new PropertyValueFactory<Proizvod, Integer>("kolicina"));
        kolonaSifra.setCellValueFactory(new PropertyValueFactory<Proizvod, Integer>("idProizvoda"));
        kolonaNaziv.setCellValueFactory(new PropertyValueFactory<Proizvod, String>("naziv"));

        ObservableList<Proizvod> oListProizvod = FXCollections.observableArrayList(listaProizvoda);
        tabelaProizvoda.setItems(oListProizvod);
    }
}
