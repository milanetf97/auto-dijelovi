create view pregled_racuna (sifra, naziv, kupljena_kolicina, cijena, ukupna_cijena, ime_radnika, datum) as
select p.sifra, p.naziv, s.kupljena_kolicina, p.cijena, s.ukupna_cijena, rd.ime, r.datum
from stavka_racuna s, proizvod p, racun r
inner join radnik rd on r.PRODAVAC_RADNIK_jmb = rd.jmb;

select * from pregled_racuna;