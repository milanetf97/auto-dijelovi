-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema auto_dijelovi
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema auto_dijelovi
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `auto_dijelovi` DEFAULT CHARACTER SET utf8mb3 ;
USE `auto_dijelovi` ;

-- -----------------------------------------------------
-- Table `auto_dijelovi`.`banka`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`banka` (
  `id_banke` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(45) NOT NULL,
  `adresa` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_banke`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`kupac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`kupac` (
  `id_kupca` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `prezime` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_kupca`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`bankovni_racun`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`bankovni_racun` (
  `broj_racuna` CHAR(16) NOT NULL,
  `stanje` INT NULL DEFAULT NULL,
  `BANKA_id_banke` INT NOT NULL,
  `KUPAC_id` INT NOT NULL,
  PRIMARY KEY (`broj_racuna`),
  INDEX `fk_BANKOVNI_RACUN_BANKA1_idx` (`BANKA_id_banke` ASC) VISIBLE,
  INDEX `fk_BANKOVNI_RACUN_KUPAC1_idx` (`KUPAC_id` ASC) VISIBLE,
  CONSTRAINT `fk_BANKOVNI_RACUN_BANKA1`
    FOREIGN KEY (`BANKA_id_banke`)
    REFERENCES `auto_dijelovi`.`banka` (`id_banke`),
  CONSTRAINT `fk_BANKOVNI_RACUN_KUPAC1`
    FOREIGN KEY (`KUPAC_id`)
    REFERENCES `auto_dijelovi`.`kupac` (`id_kupca`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`radnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`radnik` (
  `jmb` CHAR(13) NOT NULL,
  `ime` VARCHAR(45) NOT NULL,
  `prezime` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`jmb`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`vozilo_za_dostavu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`vozilo_za_dostavu` (
  `registracija` CHAR(7) NOT NULL,
  PRIMARY KEY (`registracija`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`dostavljac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`dostavljac` (
  `RADNIK_jmb` CHAR(13) NOT NULL,
  `VOZILO_ZA_DOSTAVU_registracija` VARCHAR(7) NOT NULL,
  PRIMARY KEY (`RADNIK_jmb`),
  INDEX `fk_DOSTAVLJAC_VOZILO_ZA_DOSTAVU1_idx` (`VOZILO_ZA_DOSTAVU_registracija` ASC) VISIBLE,
  CONSTRAINT `fk_DOSTAVLJAC_RADNIK1`
    FOREIGN KEY (`RADNIK_jmb`)
    REFERENCES `auto_dijelovi`.`radnik` (`jmb`),
  CONSTRAINT `fk_DOSTAVLJAC_VOZILO_ZA_DOSTAVU1`
    FOREIGN KEY (`VOZILO_ZA_DOSTAVU_registracija`)
    REFERENCES `auto_dijelovi`.`vozilo_za_dostavu` (`registracija`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`fabrika_auto_dijelova`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`fabrika_auto_dijelova` (
  `naziv` VARCHAR(20) NOT NULL,
  `adresa` VARCHAR(45) NULL DEFAULT NULL,
  `id_fabrike` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_fabrike`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`isplata_narudzbe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`isplata_narudzbe` (
  `id_isplate` INT NOT NULL AUTO_INCREMENT,
  `datum` DATE NOT NULL,
  `uplaceni_iznos` VARCHAR(45) NULL DEFAULT NULL,
  `KUPAC_id` INT NOT NULL,
  `BANKOVNI_RACUN_broj_racuna` CHAR(16) NOT NULL,
  PRIMARY KEY (`id_isplate`),
  INDEX `fk_ISPLATA_NARUDZBE_KUPAC1_idx` (`KUPAC_id` ASC) VISIBLE,
  INDEX `fk_ISPLATA_NARUDZBE_BANKOVNI_RACUN1_idx` (`BANKOVNI_RACUN_broj_racuna` ASC) VISIBLE,
  CONSTRAINT `fk_ISPLATA_NARUDZBE_BANKOVNI_RACUN1`
    FOREIGN KEY (`BANKOVNI_RACUN_broj_racuna`)
    REFERENCES `auto_dijelovi`.`bankovni_racun` (`broj_racuna`),
  CONSTRAINT `fk_ISPLATA_NARUDZBE_KUPAC1`
    FOREIGN KEY (`KUPAC_id`)
    REFERENCES `auto_dijelovi`.`kupac` (`id_kupca`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`kategorija`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`kategorija` (
  `naziv` VARCHAR(10) NOT NULL,
  `id_kategorije` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_kategorije`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`prodavac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`prodavac` (
  `RADNIK_jmb` CHAR(13) NOT NULL,
  `korisnicko_ime` VARCHAR(45) NOT NULL,
  `lozinka` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`RADNIK_jmb`),
  UNIQUE INDEX `korisnicko_ime_UNIQUE` (`korisnicko_ime` ASC) VISIBLE,
  UNIQUE INDEX `lozinka_UNIQUE` (`lozinka` ASC) VISIBLE,
  CONSTRAINT `fk_PRODAVAC_RADNIK1`
    FOREIGN KEY (`RADNIK_jmb`)
    REFERENCES `auto_dijelovi`.`radnik` (`jmb`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`narudzba`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`narudzba` (
  `id_narudzbe` INT NOT NULL AUTO_INCREMENT,
  `datum` DATE NULL DEFAULT NULL,
  `iznos` DECIMAL(6,2) NULL DEFAULT NULL,
  `KUPAC_id` INT NOT NULL,
  `DOSTAVLJAC_RADNIK_jmb` CHAR(13) NOT NULL,
  `PRODAVAC_RADNIK_jmb` CHAR(13) NOT NULL,
  PRIMARY KEY (`id_narudzbe`),
  INDEX `fk_NARUDZBA_KUPAC1_idx` (`KUPAC_id` ASC) VISIBLE,
  INDEX `fk_NARUDZBA_DOSTAVLJAC1_idx` (`DOSTAVLJAC_RADNIK_jmb` ASC) VISIBLE,
  INDEX `fk_NARUDZBA_PRODAVAC1_idx` (`PRODAVAC_RADNIK_jmb` ASC) VISIBLE,
  CONSTRAINT `fk_NARUDZBA_DOSTAVLJAC1`
    FOREIGN KEY (`DOSTAVLJAC_RADNIK_jmb`)
    REFERENCES `auto_dijelovi`.`dostavljac` (`RADNIK_jmb`),
  CONSTRAINT `fk_NARUDZBA_KUPAC1`
    FOREIGN KEY (`KUPAC_id`)
    REFERENCES `auto_dijelovi`.`kupac` (`id_kupca`),
  CONSTRAINT `fk_NARUDZBA_PRODAVAC1`
    FOREIGN KEY (`PRODAVAC_RADNIK_jmb`)
    REFERENCES `auto_dijelovi`.`prodavac` (`RADNIK_jmb`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`narudzbenica_auto_dijela`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`narudzbenica_auto_dijela` (
  `id_narudzbenice` INT NOT NULL AUTO_INCREMENT,
  `datum` DATE NULL DEFAULT NULL,
  `FABRIKA_id` INT NOT NULL,
  `prodavac_RADNIK_jmb` CHAR(13) NOT NULL,
  PRIMARY KEY (`id_narudzbenice`),
  INDEX `fk_NARUDZBENICA_AUTO_DIJELA1_idx` (`FABRIKA_id` ASC) VISIBLE,
  INDEX `fk_narudzbenica_auto_dijela_prodavac1_idx` (`prodavac_RADNIK_jmb` ASC) VISIBLE,
  CONSTRAINT `fk_NARUDZBENICA_AUTO_DIJELA1`
    FOREIGN KEY (`FABRIKA_id`)
    REFERENCES `auto_dijelovi`.`fabrika_auto_dijelova` (`id_fabrike`),
  CONSTRAINT `fk_narudzbenica_auto_dijela_prodavac1`
    FOREIGN KEY (`prodavac_RADNIK_jmb`)
    REFERENCES `auto_dijelovi`.`prodavac` (`RADNIK_jmb`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`proizvod`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`proizvod` (
  `sifra` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(45) NOT NULL,
  `kolicina` INT NULL,
  `cijena` DECIMAL(6,2) NOT NULL,
  `KATEGORIJA_id_kategorije` INT NOT NULL,
  PRIMARY KEY (`sifra`),
  INDEX `fk_PROIZVOD_KATEGORIJA1_idx` (`KATEGORIJA_id_kategorije` ASC) VISIBLE,
  CONSTRAINT `fk_PROIZVOD_KATEGORIJA1`
    FOREIGN KEY (`KATEGORIJA_id_kategorije`)
    REFERENCES `auto_dijelovi`.`kategorija` (`id_kategorije`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`racun`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`racun` (
  `id_racuna` INT NOT NULL AUTO_INCREMENT,
  `iznos` DECIMAL(6,2) NULL DEFAULT NULL,
  `datum` DATE NULL DEFAULT NULL,
  `PRODAVAC_RADNIK_jmb` CHAR(13) NOT NULL,
  PRIMARY KEY (`id_racuna`),
  INDEX `fk_RACUN_PRODAVAC1_idx` (`PRODAVAC_RADNIK_jmb` ASC) VISIBLE,
  CONSTRAINT `fk_RACUN_PRODAVAC1`
    FOREIGN KEY (`PRODAVAC_RADNIK_jmb`)
    REFERENCES `auto_dijelovi`.`prodavac` (`RADNIK_jmb`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`stavka_narudzbe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`stavka_narudzbe` (
  `broj_dijelova` INT NULL DEFAULT NULL,
  `PROIZVOD_sifra` INT NOT NULL,
  `NARUDZBA_id_narudzbe` INT NOT NULL,
  PRIMARY KEY (`PROIZVOD_sifra`, `NARUDZBA_id_narudzbe`),
  INDEX `fk_STAVKA_NARUDZBE_NARUDZBA1_idx` (`NARUDZBA_id_narudzbe` ASC) VISIBLE,
  CONSTRAINT `fk_STAVKA_NARUDZBE_NARUDZBA1`
    FOREIGN KEY (`NARUDZBA_id_narudzbe`)
    REFERENCES `auto_dijelovi`.`narudzba` (`id_narudzbe`),
  CONSTRAINT `fk_STAVKA_NARUDZBE_PROIZVOD1`
    FOREIGN KEY (`PROIZVOD_sifra`)
    REFERENCES `auto_dijelovi`.`proizvod` (`sifra`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`stavka_narudzbenice_auto_dijela`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`stavka_narudzbenice_auto_dijela` (
  `naziv_auto_dijela` VARCHAR(50) NOT NULL,
  `cijena` DECIMAL(6,2) NULL DEFAULT NULL,
  `NARUDZBENICA_AUTO_DIJELA_id_narudzbenice` INT NOT NULL,
  INDEX `fk_STAVKA_NARUDZBENICE_AUTO_DIJELA_NARUDZBENICA_AUTO_DIJELA1_idx` (`NARUDZBENICA_AUTO_DIJELA_id_narudzbenice` ASC) VISIBLE,
  CONSTRAINT `fk_STAVKA_NARUDZBENICE_AUTO_DIJELA_NARUDZBENICA_AUTO_DIJELA1`
    FOREIGN KEY (`NARUDZBENICA_AUTO_DIJELA_id_narudzbenice`)
    REFERENCES `auto_dijelovi`.`narudzbenica_auto_dijela` (`id_narudzbenice`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`stavka_racuna`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`stavka_racuna` (
  `kupljena_kolicina` INT NULL,
  `ukupna_cijena` DECIMAL(6,2) NULL,
  `RACUN_id_racuna` INT NOT NULL,
  `PROIZVOD_sifra` INT NOT NULL,
  PRIMARY KEY (`RACUN_id_racuna`, `PROIZVOD_sifra`),
  INDEX `fk_STAVKA_RACUNA_PROIZVOD1_idx` (`PROIZVOD_sifra` ASC) VISIBLE,
  CONSTRAINT `fk_STAVKA_RACUNA_PROIZVOD1`
    FOREIGN KEY (`PROIZVOD_sifra`)
    REFERENCES `auto_dijelovi`.`proizvod` (`sifra`),
  CONSTRAINT `fk_STAVKA_RACUNA_RACUN1`
    FOREIGN KEY (`RACUN_id_racuna`)
    REFERENCES `auto_dijelovi`.`racun` (`id_racuna`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`telefon`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`telefon` (
  `telefon` CHAR(9) NOT NULL,
  `fabrika_auto_dijelova_id_fabrike` INT NOT NULL,
  PRIMARY KEY (`telefon`, `fabrika_auto_dijelova_id_fabrike`),
  INDEX `fk_telefon_fabrika_auto_dijelova1_idx` (`fabrika_auto_dijelova_id_fabrike` ASC) VISIBLE,
  CONSTRAINT `fk_telefon_fabrika_auto_dijelova1`
    FOREIGN KEY (`fabrika_auto_dijelova_id_fabrike`)
    REFERENCES `auto_dijelovi`.`fabrika_auto_dijelova` (`id_fabrike`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auto_dijelovi`.`mehanicar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `auto_dijelovi`.`mehanicar` (
  `radnik_jmb` CHAR(13) NOT NULL,
  PRIMARY KEY (`radnik_jmb`),
  CONSTRAINT `fk_mehanicar_radnik1`
    FOREIGN KEY (`radnik_jmb`)
    REFERENCES `auto_dijelovi`.`radnik` (`jmb`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
