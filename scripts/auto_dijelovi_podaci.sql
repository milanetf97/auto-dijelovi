insert into RADNIK (jmb, ime, prezime) values ('1212111288884', 'Stanko', 'Barisic');
insert into RADNIK (jmb, ime, prezime) values ('1212111288885', 'Mladen', 'Pisic');
insert into RADNIK (jmb, ime, prezime) values ('1212111288886', 'Milorad', 'Dodik');
insert into RADNIK (jmb, ime, prezime) values ('1212111288887', 'Aleksandar-Aco', 'Vucic');

insert into MEHANICAR (RADNIK_jmb) value ('1212111288887');
insert into MEHANICAR (RADNIK_jmb) value ('1212111288886');
 
insert into PRODAVAC values ('1212111288884','barisa','stanko123');
 
insert into KATEGORIJA (naziv, id_kategorije) values ('filtar', null);
insert into KATEGORIJA (naziv, id_kategorije) values ('feluga', null);
insert into KATEGORIJA (naziv, id_kategorije) values ('ulje', null);
insert into KATEGORIJA (naziv, id_kategorije) values ('sijalica', null);

 
insert into PROIZVOD  values (null, 'castrol', 150, 12, 3);
insert into PROIZVOD  values (null, 'optima', 120, 17, 3);
insert into PROIZVOD  values (null, 'h7', 300, 2.5, 4);
insert into PROIZVOD  values (null, 'h4', 350, 2.5, 4);
insert into PROIZVOD  values (null, 'filtar ulja', 55, 32.20, 1);
insert into PROIZVOD  values (null, 'filtar zraka', 71, 15.85, 1);
 