delimiter $$
create procedure dodaj_stavku (in sifra_proizvoda int, in kupljena_kolicina_art int, in racun_id int) 
begin
declare postoji int default 0;
declare p_cijena decimal(6,2) default 0.0;
declare raspolozivo int default 0;

-- Provjeri ima li stavke na racunu vec
select count(*)>0 into postoji from stavka_racuna
where PROIZVOD_sifra=sifra_proizvoda and RACUN_id_racuna=racun_id;

-- Provjeri raspolozivost proizvoda
select kolicina into raspolozivo from proizvod
where sifra=sifra_proizvoda;

-- Uzmi cijenu proizvoda
select cijena into p_cijena from proizvod
where sifra=sifra_proizvoda;

if raspolozivo >= kupljena_kolicina_art then
	if postoji>0 then
		update stavka_racuna 
		set kupljena_kolicina=kupljena_kolicina+kupljena_kolicina_art, ukupna_cijena=p_cijena*kupljena_kolicina_art
		where PROIZVOD_sifra=sifra_proizvoda;
		
		update proizvod
		set kolicina=kolicina-kupljena_kolicina_art
		where sifra=sifra_proizvoda;
        
        update racun
        set iznos = iznos+p_cijena*kupljena_kolicina_art
        where id_racuna=racun_id;
	
	else
		insert into stavka_racuna values (kupljena_kolicina_art, p_cijena*kupljena_kolicina_art, racun_id, sifra_proizvoda);
		
        update racun
        set iznos = iznos+p_cijena*kupljena_kolicina_art
        where id_racuna=racun_id;
        
	end if;
end if;
end$$
delimiter ;

delimiter $$
create procedure obrisi_stavku (in sifra_proizvoda int, in kupljena_kolicina_art int, in cijena_stavke decimal(6,2), in racun_id int) 
begin

update racun
set iznos = iznos - cijena_stavke
where id_racuna = racun_id;

delete from stavka_racuna where RACUN_id_racuna = racun_id and PROIZVOD_sifra = sifra_proizvoda; 

end$$
delimiter ;

delimiter $$
create procedure dodaj_racun(in danasnji_datum date, in jmb_prodavca char(13), out id_novog_racuna int)
begin
declare novi_id int default 0;

insert into racun (id_racuna, iznos, datum, PRODAVAC_RADNIK_jmb) values (null, 0, danasnji_datum, jmb_prodavca);

select max(id_racuna) into id_novog_racuna from racun;

end$$
delimiter ;

delimiter $$
create procedure obrisi_racun(in racun_id int)
begin

-- Brisanje svih stavki sa racuna
delete from stavka_racuna where RACUN_id_racuna = racun_id;

-- Brisanje samog racuna
delete from racun where id_racuna = racun_id;

end$$
delimiter ;