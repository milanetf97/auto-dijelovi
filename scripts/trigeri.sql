create trigger smanjenje_kol_proizvoda after insert 
on stavka_racuna
for each row update proizvod
set kolicina = kolicina - new.kupljena_kolicina
where sifra = new.PROIZVOD_sifra;

create trigger povecaj_kol_proizvoda after delete
on stavka_racuna
for each row update proizvod
set kolicina = kolicina + old.kupljena_kolicina
where sifra = old.PROIZVOD_sifra;
